﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;

namespace KeithsResume.Models {
  public class PortfolioItem {
    public UInt32 PortfolioItemID {
      get;
      set;
    }

    public List<Skill> Skills {
      get;
      set;
    }

    public string ProjectName {
      get;
      set;
    }

    public string Description {
      get;
      set;
    }

    public string ProjectLink {
      get;
      set;
    }

    public string SVNLink {
      get;
      set;
    }

    public static List<PortfolioItem> GetPortfolioItems(UInt32 profileId) {
      string connectionString = ConfigurationManager.AppSettings["KeithsResumeDatabase"];
      List<PortfolioItem> xSG = new List<PortfolioItem>();

      using (MySqlConnection xConnection = new MySqlConnection(connectionString)) {
        xConnection.Open();

        using (MySqlCommand xCommand = new MySqlCommand("get_portfolioitems", xConnection)) {
          xCommand.CommandType = CommandType.StoredProcedure;
          xCommand.Parameters.Add(new MySqlParameter("var_profileid", profileId));

          using (MySqlDataReader xReader = xCommand.ExecuteReader()) {
            while (xReader.Read()) {
              xSG.Add(new PortfolioItem() {
                Description = xReader["description"] == DBNull.Value ? string.Empty : (string)xReader["description"],
                PortfolioItemID = (UInt32)xReader["portfolioitemid"],
                Skills = Skill.GetSkills((UInt32)xReader["skillgroupid"]),
                SVNLink = xReader["svnlink"] == DBNull.Value ? string.Empty : (string)xReader["svnlink"],
                ProjectLink = xReader["link"] == DBNull.Value ? string.Empty : (string)xReader["link"],
                ProjectName = xReader["projectname"] == DBNull.Value ? string.Empty : (string)xReader["projectname"]
              });
            }
          }
        }
      }

      return xSG;
    }
  }
}