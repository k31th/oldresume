﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;

namespace KeithsResume.Models {
  public class Skill {
    public UInt32 SkillID {
      get;
      set;
    }

    public string SkillName {
      get;
      set;
    }

    public string Competency {
      get;
      set;
    }

    public static List<Skill> GetSkills(UInt32 skillGroupId) {
      string connectionString = ConfigurationManager.AppSettings["KeithsResumeDatabase"];
      List<Skill> xSs = new List<Skill>();

      using (MySqlConnection xConnection = new MySqlConnection(connectionString)) {
        xConnection.Open();

        using (MySqlCommand xCommand = new MySqlCommand("get_skills", xConnection)) {
          xCommand.CommandType = CommandType.StoredProcedure;
          xCommand.Parameters.Add(new MySqlParameter("var_skillgroupid", skillGroupId));

          using (MySqlDataReader xReader = xCommand.ExecuteReader()) {
            while (xReader.Read()) {
              xSs.Add(new Skill() {
                SkillID = (UInt32)xReader["skillid"],
                SkillName = (string)xReader["skill"],
                Competency = (string)xReader["competency"]
              });
            }
          }
        }
      }

      return xSs;
    }
  }
}