﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;

namespace KeithsResume.Models {
  public class Education {
    public UInt32 EducationID {
      get;
      set;
    }

    public string SchoolTitle {
      get;
      set;
    }

    public string Location {
      get;
      set;
    }

    public string Duration {
      get;
      set;
    }

    public static List<Education> GetEducationItems(UInt32 resumeId) {
      string connectionString = ConfigurationManager.AppSettings["KeithsResumeDatabase"];
      List<Education> xEdu = new List<Education>();

      using (MySqlConnection xConnection = new MySqlConnection(connectionString)) {
        xConnection.Open();

        using (MySqlCommand xCommand = new MySqlCommand("get_educationitems", xConnection)) {
          xCommand.CommandType = CommandType.StoredProcedure;
          xCommand.Parameters.Add(new MySqlParameter("var_resumeid", resumeId));

          using (MySqlDataReader xReader = xCommand.ExecuteReader()) {
            while (xReader.Read()) {
              xEdu.Add(new Education() {
                EducationID = (UInt32)xReader["educationid"],
                SchoolTitle = (string)xReader["schooltitle"],
                Location = (string)xReader["location"],
                Duration = (string)xReader["duration"]
              });
            }
          }
        }
      }

      return xEdu;
    }
  }
}