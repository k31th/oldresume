﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;

namespace KeithsResume.Models {
  public class SkillGroup {
    public UInt32 SkillGroupID {
      get;
      set;
    }

    public string Name {
      get;
      set;
    }

    public List<Skill> Skills {
      get;
      set;
    }

    public bool ShowCompetency {
      get;
      set;
    }

    public static List<SkillGroup> GetSkillGroups(UInt32 resumeId, UInt16 skillGroupTypeId) {
      string connectionString = ConfigurationManager.AppSettings["KeithsResumeDatabase"];
      List<SkillGroup> xSG = new List<SkillGroup>();

      using (MySqlConnection xConnection = new MySqlConnection(connectionString)) {
        xConnection.Open();

        using (MySqlCommand xCommand = new MySqlCommand("get_skillgroups", xConnection)) {
          xCommand.CommandType = CommandType.StoredProcedure;
          xCommand.Parameters.AddRange(new MySqlParameter[] {
            new MySqlParameter("var_resumeid", resumeId),
            new MySqlParameter("var_skillgrouptypeid", skillGroupTypeId)
          });
          
          using (MySqlDataReader xReader = xCommand.ExecuteReader()) {
            while (xReader.Read()) {
              xSG.Add(new SkillGroup() {
                SkillGroupID = (UInt32)xReader["skillgroupid"],
                Name = (string)xReader["skillgroup"],
                Skills = Skill.GetSkills((UInt32)xReader["skillgroupid"]),
                ShowCompetency = ((UInt64)xReader["showcompetency"]) > 0 ? true : false
              });
            }
          }
        }
      }

      return xSG;
    }
  }
}