﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;

namespace KeithsResume.Models {
  public class ResumeFile {
    public string Title {
      get;
      set;
    }

    public uint ResumeFileID {
      get;
      set;
    }

    public bool Visible {
      get;
      set;
    }

    public static bool AllVisible(List<ResumeFile> resumeFiles) {
      bool allVisible =      true;

      foreach (ResumeFile xFile in resumeFiles) {
        allVisible = xFile.Visible;

        if (!allVisible) {
          break;
        }
      }

      return allVisible;
    }

    public static List<ResumeFile> GetResumeFiles(UInt32 resumeId) {
      string connectionString = ConfigurationManager.AppSettings["KeithsResumeDatabase"];
      List<ResumeFile> xResumeFiles = new List<ResumeFile>();

      using (MySqlConnection xConnection = new MySqlConnection(connectionString)) {
        xConnection.Open();

        using (MySqlCommand xCommand = new MySqlCommand("get_resumefiles", xConnection)) {
          xCommand.CommandType = CommandType.StoredProcedure;
          xCommand.Parameters.Add(new MySqlParameter("var_resumeid", resumeId));

          using (MySqlDataReader xReader = xCommand.ExecuteReader()) {
            while (xReader.Read()) {
              xResumeFiles.Add(new ResumeFile() {
                ResumeFileID = (uint)xReader["resumefileid"],
                Title = (string)xReader["title"],
                Visible = ((UInt64)xReader["visible"]) > 0 ? true : false
              });
            }
          }
        }
      }

      return xResumeFiles;
    }
  }
}