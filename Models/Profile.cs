﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Configuration;

namespace KeithsResume.Models {
  public class Profile {
    private List<PortfolioItem> _portfolioItems = null;
    private List<Resume> _resumes = null;

    public UInt32 ProfileID {
      get;
      set;
    }

    public string FirstName {
      get;
      set;
    }

    public string LastName {
      get;
      set;
    }

    public string Address {
      get;
      set;
    }

    public string City {
      get;
      set;
    }

    public string State {
      get;
      set;
    }

    public string Zip {
      get;
      set;
    }

    public string Email {
      get;
      set;
    }

    public string JobTitle {
      get;
      set;
    }

    public string Bio {
      get;
      set;
    }

    public List<Resume> Resumes {
      get {
        if (_resumes == null) {
          _resumes = new List<Resume>();
        }        

        return _resumes;
      }
    }

    public List<PortfolioItem> PortfolioItems {
      get {
        return _portfolioItems;
      }
    }

    public Profile(UInt32 profileId) {
      string connectionString = ConfigurationManager.AppSettings["KeithsResumeDatabase"];      

      using (MySqlConnection xConnection = new MySqlConnection(connectionString)) {
        xConnection.Open();

        using(MySqlCommand xCommand = new MySqlCommand("get_profile", xConnection)) {
          xCommand.CommandType = System.Data.CommandType.StoredProcedure;
          xCommand.Parameters.Add(new MySqlParameter("var_profileid", profileId));

          using (MySqlDataReader xReader = xCommand.ExecuteReader()) {
            while(xReader.Read()) { 
              ProfileID = profileId;
              FirstName = (string)xReader["firstname"];
              LastName = (string)xReader["lastname"];
              Address = (string)xReader["address"];
              City = (string)xReader["city"];
              State = (string)xReader["state"];
              Zip = (string)xReader["zip"];
              Email = (string)xReader["email"];
              JobTitle = (string)xReader["jobtitle"];
              Bio = (string)xReader["bio"];
            }
          }         
        }

        using (MySqlCommand xCommand = new MySqlCommand("get_resumes", xConnection)) {
          xCommand.CommandType = System.Data.CommandType.StoredProcedure;
          xCommand.Parameters.Add(new MySqlParameter("var_profileid", profileId));

          using (MySqlDataReader xReader = xCommand.ExecuteReader()) {
            while (xReader.Read()) {
              Resumes.Add(new Resume((uint)xReader["resumeid"]));
            }
          }
        }

        xConnection.Close();

        _portfolioItems = PortfolioItem.GetPortfolioItems(profileId);
      }
    }
  }
}