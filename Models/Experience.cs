﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;

namespace KeithsResume.Models {
  public class Experience {
    public string JobTitle {
      get;
      set;
    }

    public string Duration {
      get;
      set;
    }

    public List<ExperienceItem> JobExperiences {
      get;
      set;
    }

    public static List<Experience> GetExperiences(UInt32 resumeId) {
      string connectionString = ConfigurationManager.AppSettings["KeithsResumeDatabase"];
      List<Experience> xExps = new List<Experience>();

      using (MySqlConnection xConnection = new MySqlConnection(connectionString)) {
        xConnection.Open();

        using (MySqlCommand xCommand = new MySqlCommand("get_experiences", xConnection)) {
          xCommand.CommandType = CommandType.StoredProcedure;
          xCommand.Parameters.Add(new MySqlParameter("var_resumeid", resumeId));

          using (MySqlDataReader xReader = xCommand.ExecuteReader()) {
            while (xReader.Read()) {
              Experience xExperience = new Experience();
              xExperience.Duration = (string)xReader["duration"];
              xExperience.JobTitle = (string)xReader["jobtitle"];
              xExperience.JobExperiences = ExperienceItem.GetExperienceItems((UInt32)xReader["jobexperienceid"]);

              xExps.Add(xExperience);
            }
          }
        }
      }

      return xExps;
    }
  }
}