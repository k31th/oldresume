﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;

namespace KeithsResume.Models {
  public class Resume {    
    private List<Experience> _experiences = null;
    private List<Education> _schooling = null;
    private List<SkillGroup> _skillGroups = null;
    private List<ResumeFile> _resumeFiles = null;
    
    public UInt32 ResumeID {
      get;
      set;
    }

    public string Title {
      get;
      set;
    }    

    public List<Experience> Experiences {
      get {
        return _experiences;
      }
    }

    public List<Education> Schooling {
      get {
        return _schooling;
      }
    }

    public List<ResumeFile> Files {
      get {
        return _resumeFiles;
      }
    }

    public List<SkillGroup> SkillGroups {
      get {
        return _skillGroups;
      }
    }    

    public Resume(UInt32 resumeId) {
      ResumeID = resumeId;
      string connectionString = ConfigurationManager.AppSettings["KeithsResumeDatabase"];

      using (MySqlConnection xConnection = new MySqlConnection(connectionString)) {
        xConnection.Open();

        using (MySqlCommand xCommand = new MySqlCommand("get_resume", xConnection)) {
          xCommand.CommandType = CommandType.StoredProcedure;
          xCommand.Parameters.Add(new MySqlParameter("var_resumeid", resumeId));

          using (MySqlDataReader xReader = xCommand.ExecuteReader()) {
            if (xReader.HasRows) {
              xReader.Read();

              Title = (string)xReader["title"];
              _experiences = Experience.GetExperiences(resumeId);
              _schooling = Education.GetEducationItems(resumeId);
              _skillGroups = SkillGroup.GetSkillGroups(resumeId, (ushort)1);
              _resumeFiles = ResumeFile.GetResumeFiles(resumeId);    
            }
          }
        }

        xConnection.Close();
      }
    }
  }
}