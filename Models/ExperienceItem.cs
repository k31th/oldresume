﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;

namespace KeithsResume.Models {
  public class ExperienceItem {
    private List<ExperienceItem> _experienceItems = null;

    public UInt32 JobExperienceItemID {
      get;
      set;
    }

    public string ItemText {
      get;
      set;
    }

    public List<ExperienceItem> ExperienceItems {
      get {
        if (_experienceItems == null) {
          _experienceItems = new List<ExperienceItem>();
        }

        return _experienceItems;
      }
      set {
        _experienceItems = value;
      }
    }

    public static List<ExperienceItem> GetExperienceParentItems(UInt32 jobExperienceParentId) {
      string connectionString = ConfigurationManager.AppSettings["KeithsResumeDatabase"];
      List<ExperienceItem> xEpsItems = new List<ExperienceItem>();

      using (MySqlConnection xConnection = new MySqlConnection(connectionString)) {
        xConnection.Open();

        using (MySqlCommand xCommand = new MySqlCommand("get_experienceparentitems", xConnection)) {
          xCommand.CommandType = CommandType.StoredProcedure;
          xCommand.Parameters.Add(new MySqlParameter("var_experienceparentid", jobExperienceParentId));

          using (MySqlDataReader xReader = xCommand.ExecuteReader()) {
            while (xReader.Read()) {
              ExperienceItem xItem = new ExperienceItem();
              xItem.ItemText = (string)xReader["experienceitem"];
              xItem.JobExperienceItemID = (UInt32)xReader["jobexperienceitemid"];

              if ((Int64)xReader["hasChildren"] > 0) {
                xItem.ExperienceItems = GetExperienceParentItems(xItem.JobExperienceItemID);
              }

              xEpsItems.Add(xItem);
            }
          }
        }
      }

      return xEpsItems;
    }

    public static List<ExperienceItem> GetExperienceItems(UInt32 jobExperienceId) {
      string connectionString = ConfigurationManager.AppSettings["KeithsResumeDatabase"];
      List<ExperienceItem> xEpsItems = new List<ExperienceItem>();

      using (MySqlConnection xConnection = new MySqlConnection(connectionString)) {
        xConnection.Open();

        using (MySqlCommand xCommand = new MySqlCommand("get_experienceitems", xConnection)) {
          xCommand.CommandType = CommandType.StoredProcedure;
          xCommand.Parameters.Add(new MySqlParameter("var_jobexperienceid", jobExperienceId));

          using (MySqlDataReader xReader = xCommand.ExecuteReader()) {
            while (xReader.Read()) {
              ExperienceItem xItem = new ExperienceItem();
              xItem.ItemText = (string)xReader["experienceitem"];
              xItem.JobExperienceItemID = (UInt32)xReader["jobexperienceitemid"];

              if ((Int64)xReader["hasChildren"] > 0) {
                xItem.ExperienceItems = GetExperienceParentItems(xItem.JobExperienceItemID);
              }

              xEpsItems.Add(xItem);
            }
          }
        }
      }

      return xEpsItems;
    }
  }
}