﻿using System.Web.Mvc;
using System.Web.Routing;

namespace KeithsResume {
  public class RouteConfig {
    public static void RegisterRoutes(RouteCollection routes) {
      routes.IgnoreRoute("Download.ashx/{*pathInfo}");
      
      routes.MapRoute(
        name: "Home",
        url: "Home/",
        defaults: new { controller = "KeithsResume", action = "Home", id = UrlParameter.Optional }
      );

      routes.MapRoute(
        name: "Resume",
        url: "Resume/",
        defaults: new { controller = "KeithsResume", action = "Resume", id = UrlParameter.Optional }
      );

      routes.MapRoute(
        name: "Portfolio",
        url: "Portfolio/",
        defaults: new { controller = "KeithsResume", action = "Portfolio", id = UrlParameter.Optional }
      );

      routes.MapRoute(
        name: "Default",
        url: "{controller}/{action}/{id}",
        defaults: new { controller = "KeithsResume", action = "Home", id = UrlParameter.Optional }
      );
    }
  }
}
