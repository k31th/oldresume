﻿using MySql.Data.MySqlClient;
using System.Data;
using System.Web;

namespace KeithsResume.Handlers {
  public class ResumeFileHandler : IHttpHandler {
    public bool IsReusable {
      get {
        return false;
      }
    }

    public void ProcessRequest(HttpContext context) {
      uint resumeId = uint.Parse(context.Request["resumeId"]);
      uint resumeFileId = uint.Parse(context.Request["resumeFileId"]);
      string connectionString = System.Configuration.ConfigurationManager.AppSettings["KeithsResumeDatabase"];      

      using (MySqlConnection xConnection = new MySqlConnection(connectionString)) {
        xConnection.Open();

        using (MySqlCommand xCommand = new MySqlCommand("get_resumefile", xConnection)) {
          xCommand.CommandType = CommandType.StoredProcedure;
          xCommand.Parameters.Add(new MySqlParameter("var_resumeid", resumeId));
          xCommand.Parameters.Add(new MySqlParameter("var_resumefileid", resumeFileId));

          using (MySqlDataReader xReader = xCommand.ExecuteReader()) {
            while (xReader.Read()) {
              byte[] fileBytes = (byte[])xReader["resumefile"];
              string mimeType = (string)xReader["mimetype"];
              string fileName = (string)xReader["filename"];

              context.Response.StatusCode = 200;
              context.Response.AddHeader("Content-Disposition", "attachment;filename=\"" + fileName + "\"");
              context.Response.ContentType = mimeType;
              context.Response.BinaryWrite(fileBytes);
              context.Response.Flush();
              context.Response.End();
              context.Response.Close();
            }
          }
        }

        xConnection.Close();
      }
    }
  }
}