﻿using KeithsResume.Models;
using System.Web;
using System.Web.Mvc;

namespace KeithsResume.Helpers {
  public static class Navigation {
    public static MvcHtmlString PortfolioNavItem(PortfolioItem item) {
      var allString = string.Empty;

      if (!string.IsNullOrEmpty(item.ProjectLink)) {
        var projectLink = new TagBuilder("a");
        projectLink.MergeAttribute("target", "_blank");
        projectLink.MergeAttribute("href", item.ProjectLink);
        projectLink.SetInnerText("Project");

        allString += projectLink.ToString();
      }      

      if (!string.IsNullOrEmpty(item.SVNLink)) {
        var svnLink = new TagBuilder("a");

        if (!string.IsNullOrEmpty(item.ProjectLink) && !string.IsNullOrEmpty(item.SVNLink)) {
          svnLink.AddCssClass("indentSpace");
        }

        svnLink.MergeAttribute("target", "_blank");
        svnLink.MergeAttribute("href", item.SVNLink);
        svnLink.SetInnerText("Source");

        allString += svnLink.ToString();
      }

      return MvcHtmlString.Create(allString);
    }

    public static MvcHtmlString NavItem(string itemName, string link) {
      var navLink = new TagBuilder("a");
      navLink.SetInnerText(itemName);
      navLink.MergeAttribute("href", link);

      var navItem = new TagBuilder("li");

      var routeId = HttpContext.Current.Request.Url.AbsolutePath;

      if (routeId == link) {
        navItem.AddCssClass("active");
      }

      navItem.InnerHtml = navLink.ToString();

      return MvcHtmlString.Create(navItem.ToString(TagRenderMode.Normal));
    }
  }
}