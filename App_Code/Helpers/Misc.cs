﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace KeithsResume.Helpers {
  public static class Misc {
    public static MvcHtmlString GetGoogleAnalyticsScript() {
#if DEBUG
      return new MvcHtmlString(String.Empty);
#else
      return new MvcHtmlString(@"<script>
                                    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                                    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                                    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
                                    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

                                    ga('create', 'UA-9105868-5', 'auto');
                                    ga('send', 'pageview');

                                 </script>");      
#endif
    }
  }
}