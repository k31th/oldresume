﻿using KeithsResume.Models;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Web.Mvc;
using System.Web.UI;

namespace KeithsResume.Helpers {
  public static class Resume {
    private static void ExperienceListSubItems(HtmlTextWriter writer, List<ExperienceItem> experienceSubItems) {
      foreach (ExperienceItem xItem in experienceSubItems) {
        writer.RenderBeginTag(HtmlTextWriterTag.Li);

        writer.RenderBeginTag(HtmlTextWriterTag.Span);
        writer.Write(xItem.ItemText);
        writer.RenderEndTag();

        if (xItem.ExperienceItems.Count > 0) {
          writer.RenderBeginTag(HtmlTextWriterTag.Ul);
          ExperienceListSubItems(writer, xItem.ExperienceItems);
          writer.RenderEndTag(); //ul
        }

        writer.RenderEndTag(); //li
      }
    }

    public static MvcHtmlString ExperienceListItems(List<ExperienceItem> experienceItems) {
      StringBuilder htmlString = new StringBuilder();

      using (StringWriter xWriter = new StringWriter(htmlString)) {
        using (HtmlTextWriter writer = new HtmlTextWriter(xWriter)) {
          writer.RenderBeginTag(HtmlTextWriterTag.Ul);

          foreach (ExperienceItem xItem in experienceItems) {
            writer.RenderBeginTag(HtmlTextWriterTag.Li);

            writer.RenderBeginTag(HtmlTextWriterTag.Span);
            writer.Write(xItem.ItemText);
            writer.RenderEndTag(); //span

            if (xItem.ExperienceItems.Count > 0) {
              writer.RenderBeginTag(HtmlTextWriterTag.Ul);
              ExperienceListSubItems(writer, xItem.ExperienceItems);
              writer.RenderEndTag(); //ul
            }

            writer.RenderEndTag();
          }

          writer.RenderEndTag(); //ul;
        }
      }

      return new MvcHtmlString(htmlString.ToString());
    }
  }
}