﻿using System;
using System.Configuration;
using System.Web.Mvc;
using KRM = KeithsResume.Models;

namespace KeithsResume.Controllers {
  public class KeithsResumeController : Controller {
    private UInt32 profileId = UInt32.Parse(ConfigurationManager.AppSettings["ResumeProfileID"]);

    public ActionResult Home() {
      KRM.Profile rResume = new KRM.Profile(profileId);     
      
      return View(rResume);
    }

    public ActionResult Resume() {
      KRM.Profile rResume = new KRM.Profile(profileId);

      return View(rResume);
    }

    public ActionResult Portfolio() {
      KRM.Profile rResume = new KRM.Profile(profileId);

      return View(rResume);
    }
  }
}